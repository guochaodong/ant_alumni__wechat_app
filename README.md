# 云开发 quickstart

这是云开发的快速启动指引，其中演示了如何上手使用云开发的三大基础能力：

- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/103539_e8b4336b_5342409.jpeg "Screenshot_20210727_1030138 (1).jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/103621_7fb51a77_5342409.jpeg "Screenshot_20210727_1103038.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/104340_35a22514_5342409.jpeg "Screenshot_20210727_103913 (1).jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/104357_2606f25e_5342409.jpeg "Screenshot_20210727_103926 (1).jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/104405_105071eb_5342409.jpeg "Screenshot_20210727_103931 (1).jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/104416_ae219f04_5342409.jpeg "Screenshot_20210727_104013 (1).jpg")

## 数据库问题！！！！
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/104713_ab111800_5342409.png "搜狗截图20210727104629.png")
## 因为项目写了有一段时间了，也没有去管理，可能还会有其他问题需要各位自己解决了，也希望大家可发issue一起交流
转做集成电路了