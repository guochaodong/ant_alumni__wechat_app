const util = require('../../utils/util')
const db = wx.cloud.database()
const sport = db.collection('sport')
Page({

  data: {

  },
  onLoad: function (options) {
    sport.get().then(res => {
      for (var i = 0; i < res.data.length; i++) {
        res.data[i].price=util.formatTime(res.data[i].price)
      }
      this.setData({
        sport: res.data
      })
    })
  },
  adddetial: function () {
    wx.navigateTo({
      url: '../sportB/sportB',
      success: function (res) {},
      fail: function (err) {},
      complete: function (res) {}
    })
  }
})