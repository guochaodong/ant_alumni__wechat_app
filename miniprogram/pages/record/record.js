var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
var util = require('../../utils/util.js')
var now = new Date()
const db = wx.cloud.database()
const join = db.collection('join')
const user = db.collection('user')

var process = [];
var complete = [];

Page({
  data: {
    tabs: ["预约中", "已预约"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,

    processing: [],
    completed: [],

    openid: '',
    joinid: '',
    activity: [],
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    })
    wx.setNavigationBarTitle({
      title: '预约记录'
    })
    user.get().then(res => {
      // console.log(res)
      this.setData({
        joinid: res.data[0]._openid,
      })
    })
    wx.cloud.callFunction({
      name: 'getOpenId',
      success: res => {
        console.log(res.result.openId)
        this.setData({
          openid: res.result.openId
        })
      }
    })
  },
  onShow: function () {
    setTimeout(() => {
      join.where({
        joinid: this.data.openid
      }).get().then(res => {
        console.log(res)
        this.setData({
          activity: res.data
        })
      })
      setTimeout(() => {
        for (var i = 0; i < this.data.activity.length; i++) {
          var id = this.data.activity[i].activityid
          // console.log(id)
          join.where({
            activityid: id
          }).get().then(res => {
            // console.log(res)
            res.data[0].price = util.formatTime(res.data[0].price)
            if (util.formatTime(now) <= res.data[0].price) {
              process.push(res.data[0])
            } else {
              complete.push(res.data[0])
            }
          })
        }
        setTimeout(() => {
          this.setData({
            processing: process,
            completed: complete
          })
          setTimeout(()=>{
            if (process.length != 0 || complete.length != 0) {
              process = []
              complete = []
            }
          },3000)
          console.log(process)
          console.log(complete)
        }, 2000);
      }, 1000);
    }, 2000);
  },
  tabClick: function (e) {
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id
    });
  },

});