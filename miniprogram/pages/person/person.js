// pages/my/my.js
const db = wx.cloud.database()
const userInfo = db.collection('userInfo')
const user = db.collection('user')

Page({
  data: {
    isDisplay: true,
    _openid: '',
    userList: []
  },

  onLoad: function (options) {

    userInfo.get().then(res => {
      console.log(res.data)
      if (res.data.length != 0) {
        this.setData({
          _openid: res.data[0]._openid,
          userList: res.data,
          isDisplay: false
        })
      }
    })

  },

  getUserInfo: function (result) {
    wx.cloud.callFunction({
      name: 'getOpenId',
      complete: res => {
        console.log(res)
        var countNum = userInfo.where({
          _openid: res.result.openId
        }).count().then(res => {
          if (res.total == 0) {
            userInfo.add({
              data: result.detail.userInfo,
            }).then(res => {
              wx.navigateTo({
                url: '../personA/personA',
              })
            }).catch(err => {
              console.error(err)
            })
          } else {
            wx.navigateTo({
              url: '../person/person',
            })
          }
        });
      }
    })
  },

  loveBook: function(){
    wx.showModal({
      title:'喜欢的书',
      content:'《菜鸟是如何变得更菜的》',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },

  loveThing: function(){
    wx.showModal({
      title:'我的爱好',
      content:'吃饭，睡觉，看书，打球',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },

  loveSport: function(){
    wx.showModal({
      title:'我的运动',
      content:'羽毛球，乒乓球，网球',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },

  loveFriend: function(){
    wx.showModal({
      title:'我的朋友',
      content:'薛之谦，华晨宇，刘思鉴，隔壁老樊',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },

  myInfo:function(){
    wx.navigateTo({
      url: '../personB/personB?openid='+this.data._openid,
    })
  },

  myAssess: function(){
    wx.showModal({
      title:'我的评价',
      content:'五星分期,分12期,免息',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },
  
  versionUpdate: function(){
    wx.showModal({
      title:'版本更新',
      content:'当前版本为最新版本1.0.1',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },
  
  aboutWe: function(){
    wx.showModal({
      title:'关于我们',
      content:'文案编辑:吴树凯 \n UI编辑:文贞军 \n 项目测试:郭超东 \n 需求反馈:李能',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },
  
  shareThis: function(){
    wx.showModal({
      title:'分享邀请',
      content:'暂不支持分享邀请',
      showCancel:false,
      confirmText:'确认',
      confirmColor:'skyblue',
    })
  },
  
})