const util = require('../../utils/util')
const db = wx.cloud.database()
const join = db.collection('join')
const user = db.collection('user')
const users = []

Page({
  data: {
    userList: [],
    activityid: '',
  },
  onLoad: function (options) {
    console.log(options)
    this.setData({
      activityid: options.activityid
    })
    join.where({
      activityid: options.activityid,
    }).get().then(res => {
      console.log(res)
      for (var i = 0; i < res.data.length; i++) {
        user.where({
          _openid: res.data[i]._openid
        }).get().then(e => {
          console.log(e)
          users.push(e.data)
        })
      }
    })
  },
  bindtapLookJoinUser() {
    console.log(users)
    setTimeout(() => {
      this.setData({
        userList: users
      })
      for(var i=0;i<users.length;i++){
        users.pop()
      }
    }, 1000)

  }
})