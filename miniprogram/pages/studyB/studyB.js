const app = getApp()
const now = new Date()
const util = require('../../utils/util')
Page({
  data: {
    title: "",
    dese: "",
    show: "",
    num: "",
    price: "",
    local: "",
    others: "",
    dater: util.formatYMD(now),
    time: util.formatHM(now),

    index: 0,
    typearray: ['考研', '期末备考', '专业领域', '知识分享', '讲座', '学习经验', '辅导班', '跨专业探讨', '编程学习', '相互进步', '其他'],
    imagearry: [
      'https://i.loli.net/2020/06/05/on4HNOwtubGghyY.gif',
      'https://i.loli.net/2020/06/05/uzej8o291LI3cUA.gif',
      'https://i.loli.net/2020/06/05/zbdIEtH43h7QURL.gif',
      'https://i.loli.net/2020/06/05/vFJLBArsHX6Olya.jpg',
      'https://i.loli.net/2020/06/05/GiqoyXC82AB4cmg.gif',
      'https://i.loli.net/2020/06/05/64Pz92jFVndboJl.gif',
      'https://i.loli.net/2020/06/05/s2yZEz74LvpWJO8.gif',
      'https://i.loli.net/2020/06/05/EB1QyYU3LKMglWs.gif',
      'https://i.loli.net/2020/06/05/DgcXFOsyVrNQn5J.gif',
      'https://i.loli.net/2020/06/05/mxXuqgb6JO1QC5z.gif',
      'https://i.loli.net/2020/06/05/u3kwUZ2Ao7pYcdv.gif',
    ]
  },

  bindinputTitle: function (e) {
    this.setData({
      title: e.detail.value
    })
  },
  bindinputDese: function (e) {
    this.setData({
      dese: e.detail.value
    })
  },
  bindchangeType: function (e) {
    this.setData({
      index: e.detail.value
    })
  },
  bindinputNum: function (e) {
    this.setData({
      num: e.detail.value
    })
  },
  bindinputLoacl: function (e) {
    this.setData({
      local: e.detail.value
    })
  },
  bindchangeDate: function (e) {
    this.setData({
      dater: e.detail.value
    })
  },
  bindchangeTime: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  bindinputOthers: function (e) {
    this.setData({
      others: e.detail.value
    })
  },

  sumbit: function (e) {
    const db = wx.cloud.database()
    db.collection('study').add({
      data: {
        title: this.data.title,
        dese: this.data.dese,
        show: this.data.typearray[this.data.index],
        num: this.data.num,
        price: new Date(this.data.dater+' '+this.data.time),
        local: this.data.local,
        others: this.data.others,
        image: this.data.imagearry[this.data.index]
      },
      success: res => {
        console.log(res)
        this.setData({
        })
        wx.showToast({
          title: '信息发布成功',
          icon: 'success',
          duration: 2000
        })
        console.log('新增数据成功，记录_id:', res._id)
        wx.redirectTo({
          url: '../study/study',
        })
      },
      fail: err => {
        wx.showToast({
          title: '信息发布失败',
          icon: 'none'
        })
        console.error('信息发布失败', err)
      }
    })
  },
})