const util = require('../../utils/util')
const db = wx.cloud.database()
const canteen = db.collection('canteen')
Page({
  data: {

  },
  onLoad: function (options) {
    canteen.get().then(res => {
      console.log(res)
      for (var i = 0; i < res.data.length; i++) {
        res.data[i].price = util.formatTime(res.data[i].price)
      }
      this.setData({
        canteen: res.data
      })
    })
  },
  adddetial: function () {
    wx.navigateTo({
      url: '../canteenB/canteenB',
      success: function (res) {},
      fail: function (err) {},
      complete: function (res) {}
    })
  }
})