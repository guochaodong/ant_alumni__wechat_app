const util = require('../../utils/util')
const db = wx.cloud.database()
const study = db.collection('study')
const userInfo = db.collection('userInfo')
const user = db.collection('user')
const join = db.collection('join')

Page({
  data: {
    activityid: '',
    joinid: '',
    willjoinid:'',
    study: [],
    userList: [],
    user: [],
    count: 1,
  },
  onLoad: function (options) {
    console.log(options)
    study.doc(options._id).get({
      success: res => {
        // console.log(res)
        res.data.price = util.formatTime(res.data.price)
        this.setData({
          study: res.data,
          activityid: res.data._id
        })
      }
    })
    userInfo.where({
      _openid: options._openid
    }).get().then(res => {
      // console.log(res.data[0])
      this.setData({
        userList: res.data[0],
      })
    })
    user.where({
      _openid: options._openid
    }).get().then(res => {
      // console.log(res.data)
      this.setData({
        user: res.data,
        joinid: res.data[0]._openid,
      })
    })
    wx.cloud.callFunction({
      name:'getOpenId',
      success:res=>{
        console.log(res)
        this.setData({
          willjoinid:res.result.openId
        })
      }
    })
  },
  onSubmit: function () {
    join.where({
      activityid: this.data.activityid,
      joinid: this.data.willjoinid
    }).count({
      success: res => {
        console.log(res)
        this.setData({
          count: res.total
        })
        console.log(this.data.count)

        if (this.data.count != 0) {
          wx.showToast({
            title: '你已预约组队',
          })
        } else {
          join.add({
            data: {
              activityid: this.data.activityid,
              joinid: this.data.willjoinid,
              title: this.data.study.title,
              dese: this.data.study.dese,
              show: this.data.study.show,
              num: this.data.study.num,
              price: new Date(this.data.study.price),
              local: this.data.study.local,
              others: this.data.study.others
            },
            success: res => {
              wx.showToast({
                title: '预约成功',
                icon: 'success',
                duration: 2000
              })
              wx.navigateTo({
                url: '../order/order?activityid=' + this.data.activityid + "&name=" + this.data.user[0].username,
              })
              console.log('新增数据成功，记录_id:', res._id)
            },
            fail: err => {
              wx.showToast({
                title: '信息发布失败',
                icon: 'none'
              })
              console.error('信息发布失败', err)
            }
          })
        }
      }
    })

  }
})