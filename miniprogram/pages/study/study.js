var util = require('../../utils/util.js')
const db = wx.cloud.database()
const studyCollection = db.collection('study')
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    studyCollection.get().then(res => {
      console.log(res)
      for (var i = 0; i < res.data.length; i++) {
        res.data[i].price = util.formatTime(res.data[i].price)
      }
      this.setData({
        study: res.data
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  adddetial: function () {
    wx.navigateTo({
      url: '../studyB/studyB',
      success: function (res) {},
      fail: function (err) {},
      complete: function (res) {}
    })
  }
})